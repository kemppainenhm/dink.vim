" based on eink, with less crap

hi clear
if exists("syntax_on")
    syntax reset
endif

let colors_name = "dink"

" undo default crap
hi Constant     cterm=NONE          ctermbg=NONE    ctermfg=NONE
hi Number       cterm=NONE          ctermbg=NONE    ctermfg=NONE
hi Identifier   cterm=NONE          ctermbg=NONE    ctermfg=NONE
hi Special      cterm=NONE                          ctermfg=NONE

" diff colors are ok
hi diffAdded    cterm=bold                          ctermfg=28
hi diffRemoved  cterm=bold                          ctermfg=1

" dim comments
hi PreProc      cterm=NONE                          ctermfg=NONE
hi Comment      cterm=NONE                          ctermfg=242

" bold
hi Statement    cterm=bold                          ctermfg=237
hi Type         cterm=bold                          ctermfg=237
hi Label        cterm=bold                          ctermfg=237
hi String       cterm=bold                          ctermfg=237
hi MatchParen   cterm=bold          ctermbg=250     ctermfg=NONE

" reverse
hi Visual       cterm=reverse                       ctermfg=NONE

" use a less offensive background color for hilighting errors, todos
hi Error        cterm=NONE          ctermbg=49      ctermfg=NONE
hi ErrorMsg     cterm=NONE          ctermbg=49      ctermfg=NONE
hi Todo         cterm=NONE          ctermbg=49      ctermfg=NONE

