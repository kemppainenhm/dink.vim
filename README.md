dink.vim, a minimalistic colorscheme for vim
============================================

Dink is mostly grayscale, and does not override your terminal's chosen
background or primary text color.

![](dink.png)

## Installation

0. (Prerequisite) Use a terminal with light background for writing C code
1. Download the raw version of
[`dink.vim`](https://gitlab.com/kemppainenhm/dink.vim/raw/master/dink.vim)
and save it in `~/.vim/colors/`
2. Stick `colorscheme dink` in `~/.vimrc`

## Why

Fruity lexical hilighting makes no sense to me and I've been
calling syntax off if I ever need vim in place of nvi.  However, I do find that
colors make it easier to read diffs (git commit -v) and I don't mind dimming
dead code (#if 0 blocks) or comments.  Also catching unterminated strings
before running a compiler is convenient.

So what I really want is a minimalistic colorscheme that does just that and
not much else.  I looked around and while there were lots of colorschemes that
claim to be minimalistic, they all ended up being much larger than needed,
and damn well near all of them forced a particular background & foreground
color combination (I have a perfectly good choice of foreground & background
colors set in my xterm, thank you very much).  Maybe there's one I'd like out
there, but trying them all is too much effort.

I took [`eink.vim`](https://github.com/kisom/eink.vim), tweaked a couple things
and removed the parts I don't like or need.

I'm also bolding some keywords, which looks disgusting in .vimrc and many
other contexts, but somewhat bearable in C code.  I might get sick of it and
turn it off later.

## Credit

[Thank kyle, jcs, qbit.](https://github.com/kisom/eink.vim)